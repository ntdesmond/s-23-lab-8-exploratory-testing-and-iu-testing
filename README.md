# Lab 8 - Exploratory testing and UI

See tests report in [TESTS.md](TESTS.md).

Tests pipeline status: 
[![pipeline status](https://gitlab.com/ntdesmond/s-23-lab-8-exploratory-testing-and-iu-testing/badges/master/pipeline.svg)](https://gitlab.com/ntdesmond/s-23-lab-8-exploratory-testing-and-iu-testing/-/commits/master) 

## Introduction

Hey, hey, hey, what's up guys, it is almost the last lab for the course, here we go. Imagine your product is done, you've tested and analysed almost everything you could, and here we have the last step in our process: UI and exploratory testing.  **Let's roll!**

## Exploratory testing

Basically, if you want to do exploratory testing, you need to pick a use case, for example to book a ticket for "Star Wars XX: THE REVENGE OF THE VENGEANCE SKYWALKER SAGA FINAL" in a cinema, and you need to describe the path you need to walk through to get what you want, marking all errors or inconsistencies you've found in the way and document each step passed.

## UI testing

Ok, after you've done the previous step and you understand what actions you need to take to achieve your target you may test your UI to check that each critical component is in place for you to accomplish the needed User Story. Usually Selenium is used - it is a multi-language library which allows to do Unit Testing, walk through website components and imitate human behaviour. In some sense, it is automated Exploratory testing.

## Lab

Ok, let's crush all the stuff out of it:
1. Create your fork of the `Lab 9 - Exploratory testing and UI` repo and clone it. [***Here***](https://gitlab.com/sqr-inno/s23-lab8-exploratory-testing-and-ui-testing)
2. There are a lot of different ways to do the exploratory testing, such as:
   + Landmark tour (visit the most important features, just walking through use cases)
   + Antisocial tour (break everything that moves, handle yourself as a real app user, enter anything anywhere)
   + Supermodel tour (check the UI of the app on each tool, any insufficiencies with requirements or with common sense)
   + etc, there is a lot of them...
3. We are going to work only with Landmark tours. Here is how you do it: develop the Use Case (or pick one, if you already have them), next you should create a table like this for each path you are taking to accomplish the described use case:
![table image](https://i.ibb.co/DgStckd/landmark-testing.png)
4. Ok, after you've created exploratory tests, it is a legitimate thought to automate some of your testing, especially for the most used and critical paths, like login, core functionality, etc. To do it, there exists a whole family of different frameworks; we are going to use Selenium for that purpose. Selenium is a whole set of products which allows your program to run a website/application, parse them and accomplish different activities.
   + First, we will need to download and install/unpack a webdriver, which we are going to use to imitate our browser. We are going to use the Firefox driver - gecko. You can dowload latest verion [here](https://github.com/mozilla/geckodriver/releases)
   + Next, if you are using Linux, we need to add our webdriver to the system paths, you may do it like this:
     ```sh
     export PATH=$PATH:_path_to_webdriver_
     ```
   + Ok, we are done with preps, now we can start the fun part, let's create a `test.py` file in the test directory. It should look like this:
     ```python
      from selenium import webdriver
      from selenium.webdriver.common.keys import Keys
      from selenium.webdriver.common.by import By

      driver = webdriver.Firefox()
      driver.implicitly_wait(10) # seconds

      driver.get("http://google.com")
      print("Page title is: "+driver.title)
      assert "Google" in driver.title

      elem = driver.find_element(By.NAME, "q")
      elem.clear()
      elem.send_keys("SQR course is the best")
      elem.send_keys(Keys.RETURN)


      elem = driver.find_element(By.ID, "search")
      assert elem.is_displayed()

      elem = driver.find_element(By.NAME, "q")
      assert elem.get_attribute("value") == "SQR course is the best"
      assert "SQR course is the best" in driver.title

      driver.close()
     ```
That's it, we may run our tests with `python3 test.py` and it should autotest Google's webpage, and we're done. Selenium has bindings for many popular programming languages, so you can use the one you wish.

## Homework

As a homework you need to pick any website/application you want. Mention the website/app in your work, and check that works submitted(even without ready for review label) before you; your choice should be unique, one student - one application. After you've chosen your application to test, develop at least three exploratory test cases with a complete description of what you've tested. Then you will need to fully automate at least one of the tests using Selenium and push your code to GitLab together with the link to your Exploratotry tests (or the tests themselves).

### Extra
I will put one more point if your Selenium test automation would be in GitLab CI and there would not be a driver binary in the repository.
