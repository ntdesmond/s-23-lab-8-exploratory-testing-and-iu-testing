import pytest
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver

from helpers import hover_all_click_last, wait_for_tab_count, wait_for_element, wait_for_new_page

base_url = "https://innopolis.university/en/"


@pytest.fixture
def chrome_options(chrome_options):
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_argument('--ignore-ssl-errors=yes')
    chrome_options.add_argument('--ignore-certificate-errors')
    chrome_options.add_argument("--window-size=1920,1080")
    chrome_options.add_argument("--disable-dev-shm-usage")

    return chrome_options


def test_manuel_mazzara_in_faculty_members(selenium: WebDriver):
    selenium.get(base_url)
    assert selenium.title == "Innopolis University"

    nav_link = selenium.find_element(
        By.XPATH,
        "//span[contains(., 'University') and starts-with(@class, 'nav')]"
    )
    faculty_link = selenium.find_element(
        By.XPATH,
        "//a[contains(., 'Academic staff') and starts-with(@class, 'sub-nav')]"
    )

    hover_all_click_last(selenium, [nav_link, faculty_link])
    wait_for_element(selenium, By.CLASS_NAME, "full-time-professors-result")
    assert selenium.title == "Academic staff"

    professors = [
        title.text for title in
        selenium.find_elements(
            By.CSS_SELECTOR,
            "h5.professors-card-info-title"
        )
    ]
    assert "Manuel Mazzara" in professors


def test_bachelor_scholarship_is_490_usd(selenium: WebDriver):
    selenium.get(base_url)
    assert selenium.title == "Innopolis University"

    nav_link = selenium.find_element(
        By.XPATH,
        "//span[contains(., 'Education') and starts-with(@class, 'nav')]"
    )
    higher_education_link = selenium.find_element(
        By.XPATH,
        "//a[contains(., 'Higher education') and starts-with(@class, 'sub-nav')]"
    )
    bachelors_link = selenium.find_element(
        By.XPATH,
        "//a[contains(., 'Bachelor')]"
    )

    first_tab = selenium.current_window_handle

    hover_all_click_last(selenium, [nav_link, higher_education_link, bachelors_link])
    wait_for_tab_count(selenium, 2)

    new_tab = next(tab for tab in selenium.window_handles if tab != first_tab)
    selenium.switch_to.window(new_tab)

    wait_for_element(selenium, By.CLASS_NAME, "this-year-receiving-company")
    assert selenium.title == "Bachelor"

    scholarship = selenium.find_element(
        By.XPATH,
        "//div[contains(., 'scholarship') and contains(@class, 'info-item')]"
    ).text
    assert "490 USD" in scholarship


def test_succi_is_still_there(selenium: WebDriver):
    selenium.get(base_url)
    assert selenium.title == "Innopolis University"

    nav_link = selenium.find_element(
        By.XPATH,
        "//span[contains(., 'Research') and starts-with(@class, 'nav')]"
    )
    faculties_link = selenium.find_element(
        By.XPATH,
        "//a[contains(., 'Faculties') and starts-with(@class, 'sub-nav')]"
    )
    computer_science_link = selenium.find_element(
        By.XPATH,
        "//a[contains(., 'Computer Science')]"
    )

    first_tab = selenium.current_window_handle

    hover_all_click_last(selenium, [nav_link, faculties_link, computer_science_link])
    wait_for_tab_count(selenium, 2)

    new_tab = next(tab for tab in selenium.window_handles if tab != first_tab)
    selenium.switch_to.window(new_tab)

    wait_for_element(selenium, By.CLASS_NAME, "uni-team-faculty")
    assert selenium.title == "Faculty of Computer Science and Engineering"

    team_member = selenium.find_element(
        By.XPATH,
        "//div[starts-with(@class, 'team-member')]"
    ).text
    assert "Giancarlo Succi" in team_member