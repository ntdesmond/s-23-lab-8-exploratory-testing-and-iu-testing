# Lab 8 - Exploratory testing and ~~UI~~ IU

I tested [innopolis.university](http://innopolis.university) website.
Below is the test report.

## Test 1: Mazzara is among the faculty members

| Action | Status/Comment |
| - | - |
| Went to innopolis.university and got "Innopolis University" page | ✅ |
| Hovered on navigation menu and clicked on University → Academic staff  | ✅ |
| "Academic staff" page rendered | ✅ |
| Looked through professors' cards and found Manuel Mazzara | ✅ |

## Test 2: Found information on scholarship in IU

| Action | Status/Comment |
| - | - |
| Went to innopolis.university and got "Innopolis University" page | ✅ |
| Hovered on navigation menu and clicked on Education → Higher education → Bachelor's program | ✅ |
| New tab opened and "Bachelor" page rendered | ❔ Opening a new tab might be redundant |
| Found scholarship info on the page (490 USD monthly) | ❗ Information is outdated |

## Test 3: Giancarlo Succi is still a faculty member

| Action | Status/Comment |
| - | - |
| Went to innopolis.university and got "Innopolis University" page | ✅ |
| Hovered on navigation menu and clicked on Research → Faculties → Faculty of Computer Science and Engineering | ✅ |
| New tab opened and "Faculty of Computer Science and Engineering" page rendered | ❔ Opening a new tab might be redundant |
| Found Giancarlo Succi presented as the faculty Dean on the page | ❗ Information is outdated |
