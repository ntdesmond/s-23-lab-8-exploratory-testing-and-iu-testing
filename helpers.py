from typing import Iterable

from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


def hover_all_click_last(selenium: WebDriver, elements: Iterable[WebElement]):
    actions = ActionChains(selenium)
    for element in elements:
        actions.move_to_element(element).pause(0.1).perform()
    actions.click().perform()


def wait_for_new_page(selenium: WebDriver):
    old_page = selenium.find_element(By.TAG_NAME, 'html')
    wait = WebDriverWait(selenium, 2)
    wait.until(expected_conditions.staleness_of(old_page))


def wait_for_tab_count(selenium: WebDriver, target_tab_count: int):
    wait = WebDriverWait(selenium, 2)
    wait.until(expected_conditions.number_of_windows_to_be(target_tab_count))


def wait_for_element(selenium: WebDriver, by: By, value: str):
    wait = WebDriverWait(selenium, 2)
    wait.until(expected_conditions.presence_of_element_located((by, value)))